package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {
    List <Shape> children = new ArrayList <>();

    public NestingShape() {
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int DeltaX, int DeltaY, int width, int height) {
        super(x, y, DeltaX, DeltaY, width, height);
    }

    public void move(int width, int height) {
//Remember that the NestingShape’s children only want
//to move within the bounds of the NestingShape itself, rather than the whole screen.
        super.move(width, height);

        for (Shape child : children) {
            child.move(this.fWidth, this.fHeight);
        }
    }

    @Override
    public void paint(Painter painter) {
//Paints a NestingShape object by drawing a rectangle around the edge of its
//bounding box. The NestingShape object's children are then painted.
        painter.drawRect(this.fX, this.fY, this.fWidth, this.fHeight);

        painter.translate(this.fX, this.fY);

        for (Shape child : children) {
            child.paint(painter);
        }

        painter.translate(-this.fX, -this.fY);
    }

    public void add(Shape child) throws IllegalArgumentException {
        if(child.parent!=null) {
            throw new IllegalArgumentException();
        } else if ((child.getHeight()+child.getY())>this.getHeight()||(child.getWidth()+child.getX())>this.getWidth()) {
            throw new IllegalArgumentException();
        }
        children.add(child);
        child.parent = this;
    }

    public void remove(Shape child) {
            children.remove(child);
            child.parent = null;
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
            return children.get(index);
    }

    public int shapeCount() {
        return children.size();
    }

    public int indexOf(Shape child) {
        return children.indexOf(child);
    }

    public boolean contains(Shape child) {
        return children.contains(child);
    }
}
