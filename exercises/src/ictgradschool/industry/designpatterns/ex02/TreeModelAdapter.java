package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import java.util.Iterator;

public class TreeModelAdapter {
    private Shape _adaptee;
    public TreeModelAdapter (Shape root) {
        _adaptee = root;
    }

    public Object getRoot() {
        return _adaptee;
    }

    public Object getChildCount(Object parent) {
        Shape result = null;

        if(parent instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) parent;
            result = nestingShape.getNumberOfChildren();
        }
        return result;
    }

    public Object getChild(Object parent, int index) {
        Object result = null;

        if(parent instanceof Shape) {
            Shape sha = (Shape) parent;
            result = sha.getNestingShape();
        }
        return result;
    }
    public int getIndexOfChild(Object parent, Object children) {
        int indexOfChild = -1;

        if(parent instanceof Shape) {
            Shape sha = (Shape) parent;
            Iterator <NestingShape> i = sha.getIterator();
            boolean found = false;

            int index =0;
            while(!found && i.hasNext()) {
                NestingShape current = i.next();
                if(child == current) {
                    found = true;
                    indexOfChild = index;
                } else {
                    index++
                }
            }
        }
    } return indexOfChild;
}
